#[macro_use]
extern crate text_io;

fn main() { 
	loop{
		println!("Would you like to divide(/), multiply(*) add(+) or subtract(-)?");
		let operator: String = read!();
		match operator.as_str() {
			"/" => {
				println!("You have chosen divide");
				divide();
			},
			"*" => {
				println!("You have chosen multiply");
				multiply();
			},
			"-" => {
				println!("You have chosen subtract");
				subtract();
			},
			"+" => {
				println!("You have chosen add");
				add();
			},
			_ => {
				println!("Incorrect input '{}', please choose /, *, + or -.", operator);
				main();
			}
		}
		quit();
	}	
}

fn divide() {
	println!("Please input the number that you want to divide");
	let div1: i32 = read!();

	println!("Please input the number you want to divide by");
	let div2: i32 = read!();

	let dresult = div1 / div2;
	println!("The result is: {}", dresult);
}

fn multiply() {
	println!("Please input the number you would like to multiply");
	let mul1: i32 = read!();

	println!("Please input the number you want to multiply by");
	let mul2: i32 = read!();

	let mresult = mul1 * mul2;
	println!("The result is: {}", mresult);
}

fn add() {
	println!("Please input the 1st number you would like to add");
	let add1: i32 = read!();

	println!("Please input the 2nd number you would like to add");
	let add2: i32 = read!();

	let aresult = add1 + add2;
	println!("The result is: {}", aresult);
}

fn subtract() {
	println!("Please input the 1st number you would like to to subtract");
	let sub1: i32 = read!();

	println!("Please input the 2nd number you would like to subtract");
	let sub2: i32 = read!();

	let sresult = sub1 - sub2;
	println!("The result is: {}", sresult);
}
fn quit() {
	println!("Would you like to repeat or quit? [r/q]");
	let repeat: String = read!();
	match repeat.as_str() {
		"r" => {
			main();
		}
		"q" => {
			std::process::exit(1);
		}
		_ => {
			println!("Incorrect input, please choose r, or q.");
			quit();
		}
	}
}
